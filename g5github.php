<?php
namespace Grav\Plugin;

use Grav\Common\Plugin;
use Grav\Common\Page\Page;
use Grav\Common\Grav;

class G5GithubPlugin extends Plugin
{
    protected $active = false;
    protected $g5github;

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'onPluginsInitialized' => ['onPluginsInitialized', 0]
        ];
    }

    /**
     * Initialize configuration
     */
    public function onPluginsInitialized()
    {
        if ($this->isAdmin()) {
            $this->active = false;
            return;
        }

        $this->enable([
            'onTwigSiteVariables' => ['onTwigSiteVariables', 0]
        ]);
    }


    /**
     * Make form accessible from twig.
     */
    public function onTwigSiteVariables()
    {
        require_once $this->grav['locator']->findResource('plugins://github/vendor/autoload.php');

        $cache = $this->grav['cache'];
        $cache_id = md5('relatedpages'.$cache->getKey());
        $this->g5github = $cache->fetch($cache_id);

        // not found in cache, get the stuff again
        if ($this->g5github === false) {

            $config = $this->config->get('plugins.g5github');

            $client = new \Github\Client(
                new \Github\HttpClient\CachedHttpClient(array('cache_dir' => CACHE_DIR . '/github'))
            );

            $repo = $client->api('repo');

            try {
                $this->g5github['releases'] = $repo->releases()->all($config['user'], $config['repo']);
                if ($config['show_stars']) {
                    $this->g5github['stars'] = $repo->show($config['user'], $config['repo'])['stargazers_count'];
                }

                $cache->save($cache_id, $this->g5github, $config['lifetime']);
            } catch(\Exception $e) {
                $this->g5github['error'] = $e->getMessage();
            }
        }

        $this->grav['twig']->twig_vars['g5github'] = $this->g5github;
    }
}
